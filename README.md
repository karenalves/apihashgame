# API Hash Gamer

#####################################################
Informações básicas
#####################################################
	
	Aplicação construída em Asp .Net Core 2.2
	Utilizado Swagger para documentação da API
	
#####################################################
Início da Partida
#####################################################

**** POST

/api/Games

Para iniciar a partida, é somente executar a requisição
e será retornado o Id da partida iniciada, juntamente
com o primeiro jogador.

{
  "id": "8036ff58-2670-4b41-b438-7dcd19fb2781",
  "firstPlayer": "O"
}


###############################################
Jogar
###############################################

*** POST

/api/Games/{game.id}/movement

Para realizar as jogadas, deverá ser informado o
Id da partida, o jogador da vez, e a posição que
deseja marcar:

{
  "id": "[guid_id]",
  "player": "[string]",
  "position": {
    "x": [int],
    "y": [int]
  }
}

EX:
{
  "id": "8036ff58-2670-4b41-b438-7dcd19fb2781",
  "player": "O",
  "position": {
    "x": 0,
    "y": 0
  }
}

{
  "id": "8036ff58-2670-4b41-b438-7dcd19fb2781",
  "player": "X",
  "position": {
    "x": 1,
    "y": 0
  }
}

