﻿using HashGame.Models;
using System;

namespace HashGame.Services
{
    public interface IGameService
    {
        bool CheckPosition(string[,] GameBoard, Position newPosition);

        string CheckEndGame(string[,] GameBoard, Position newPosition);

        string[,] MarkedPosition(string[,] GameBoard, Position newPosition, string player);
        bool CheckGameInProgress(string[,] GameBoard);
    }
}
