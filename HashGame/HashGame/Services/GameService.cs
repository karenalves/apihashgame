﻿using HashGame.Models;
using System;

namespace HashGame.Services
{
    public class GameService : IGameService
    {
        public bool CheckPosition(string[,] GameBoard, Position newPosition)
        {
            if (newPosition.X == 0 && newPosition.Y == 2)
                return GameBoard[0, 0] == null ? true : false;
            else if (newPosition.X == 1 && newPosition.Y == 2)
                return GameBoard[0, 1] == null ? true : false;
            else if (newPosition.X == 2 && newPosition.Y == 2)
                return GameBoard[0, 2] == null ? true : false;
            else if (newPosition.X == 0 && newPosition.Y == 1)
                return GameBoard[1, 0] == null ? true : false;
            else if (newPosition.X == 1 && newPosition.Y == 1)
                return GameBoard[1, 1] == null ? true : false;
            else if (newPosition.X == 2 && newPosition.Y == 1)
                return GameBoard[1, 2] == null ? true : false;
            else if (newPosition.X == 0 && newPosition.Y == 0)
                return GameBoard[2, 0] == null ? true : false;
            else if (newPosition.X == 1 && newPosition.Y == 0)
                return GameBoard[2, 1] == null ? true : false;
            else
                return GameBoard[2, 2] == null ? true : false;
        }

        public string[,] MarkedPosition(string[,] GameBoard, Position newPosition, string player)
        {
            if (newPosition.X == 0 && newPosition.Y == 2)
                GameBoard[0, 0] = player;
            else if (newPosition.X == 1 && newPosition.Y == 2)
                GameBoard[0, 1] = player;
            else if (newPosition.X == 2 && newPosition.Y == 2)
                GameBoard[0, 2] = player;
            else if (newPosition.X == 0 && newPosition.Y == 1)
                GameBoard[1, 0] = player;
            else if (newPosition.X == 1 && newPosition.Y == 1)
                GameBoard[1, 1] = player;
            else if (newPosition.X == 2 && newPosition.Y == 1)
                GameBoard[1, 2] = player;
            else if (newPosition.X == 0 && newPosition.Y == 0)
                GameBoard[2, 0] = player;
            else if (newPosition.X == 1 && newPosition.Y == 0)
                GameBoard[2, 1] = player;
            else if (newPosition.X == 2 && newPosition.Y == 0)
                GameBoard[2, 2] = player;

            return GameBoard;
        }

        public string CheckEndGame(string[,] GameBoard, Position newPosition)
        {
            if ((CheckEquals(GameBoard[0, 0], GameBoard[0, 1], GameBoard[0, 2]) == GameBoard[0, 0]) && GameBoard[0, 0] != null)
                return GameBoard[0, 0];
            else if ((CheckEquals(GameBoard[1, 0], GameBoard[1, 1], GameBoard[1, 2]) == GameBoard[1, 0]) && GameBoard[1, 0] != null)
                return GameBoard[1, 0];
            else if ((CheckEquals(GameBoard[2, 0], GameBoard[2, 1], GameBoard[2, 2]) == GameBoard[2, 0]) && GameBoard[2, 0] != null)
                return GameBoard[2, 0];
            else if ((CheckEquals(GameBoard[0, 0], GameBoard[1, 0], GameBoard[2, 0]) == GameBoard[0, 0]) && GameBoard[0, 0] != null)
                return GameBoard[0, 0];
            else if ((CheckEquals(GameBoard[0, 1], GameBoard[1, 1], GameBoard[2, 1]) == GameBoard[0, 1]) && GameBoard[0, 1] != null)
                return GameBoard[0, 1];
            else if ((CheckEquals(GameBoard[0, 2], GameBoard[1, 2], GameBoard[2, 2]) == GameBoard[0, 2]) && GameBoard[0, 2] != null)
                return GameBoard[0, 2];
            else if ((CheckEquals(GameBoard[0, 0], GameBoard[1, 1], GameBoard[2, 2]) == GameBoard[0, 0]) && GameBoard[0, 0] != null)
                return GameBoard[0, 0];
            else if ((CheckEquals(GameBoard[0, 2], GameBoard[1, 1], GameBoard[2, 0]) == GameBoard[0, 2]) && GameBoard[0, 2] != null)
                return GameBoard[0, 2];
            else
                return null;
        }

        public bool CheckGameInProgress(string[,] GameBoard)
        {
            for (int i = 0; i < GameBoard.GetLength(0); i++)
            {
                for (int j = 0; j < GameBoard.GetLength(1); j++)
                {
                    if (GameBoard[i, j] == null)
                        return false;
                }
            }

            return true;
        }

        public string CheckEquals(string a, string b, string c)
        {
            if (a != null && b != null && c != null)
                return a.Equals(b) && b.Equals(c) ? a : null;

            return null;
        }
    }
}
