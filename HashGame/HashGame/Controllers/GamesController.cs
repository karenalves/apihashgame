﻿using AutoMapper;
using HashGame.BindingModel;
using HashGame.Models;
using HashGame.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HashGame.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : Controller
    {
        private static Game game;
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;

        public GamesController(IMapper mapper, IGameService gameService)
        {
            _mapper = mapper;
            _gameService = gameService;
        }

        // POST api/games
        [HttpPost]
        public IActionResult Post()
        {
            game = new Game();
            var firstPlayer = "XO";
            Random random = new Random();

            game.PlayerMovements.Add(Convert.ToString(firstPlayer[random.Next(firstPlayer.Length)]));

            return Ok(_mapper.Map<CreateGameBindingModel>(game));
        }

        // POST api/games/{game.id}/movement
        [HttpPost("{game.id}/movement")]
        public IActionResult PostMovement(PlayGameBindingModel PlayGameBindingModel)
        {
            if (game != null)
            {
                if (!PlayGameBindingModel.Id.Equals(game.Id))
                {
                    return NotFound(new { Msg = "Partida não encontrada." });
                }
            }
            else
            {
                return NotFound(new { Msg = "Partida não Iniciada. Favor iniciar uma partida." });
            }
            

            if (game.PlayerMovements.Count == 1)
            {
                if (!PlayGameBindingModel.Player.Equals(game.PlayerMovements[game.PlayerMovements.Count - 1]))
                {
                    return NotFound(new { Msg = "Não é turno do jogador." });
                }
            }
            else
            {
                if (PlayGameBindingModel.Player.Equals(game.PlayerMovements[game.PlayerMovements.Count - 1]))
                {
                    return NotFound(new { Msg = "Não é turno do jogador." });
                }
            }

            if (!_gameService.CheckPosition(game.GameBoard, PlayGameBindingModel.Position))
            {
                return NotFound(new { Msg = "Posição já contém valor. Favor escolher outra posição para realizar a jogada." });
            }

            game.GameBoard = _gameService.MarkedPosition(game.GameBoard, PlayGameBindingModel.Position, PlayGameBindingModel.Player);
            game.PlayerMovements.Add(PlayGameBindingModel.Player);

            var winner = _gameService.CheckEndGame(game.GameBoard, PlayGameBindingModel.Position);

            if (winner == null && !_gameService.CheckGameInProgress(game.GameBoard))
            {
                return Ok(new { Msg = "Jogada realizada com sucesso." });
            }
            else
            {
                game = null;

                if (winner == null)
                {
                    return Ok(new { Status = "Partida finalizada", Winner = "Draw" });
                }

                return Ok(new { Msg = "Partida finalizada", Winner = winner });
            }
        }
    }
}