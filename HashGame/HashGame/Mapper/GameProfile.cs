﻿using AutoMapper;
using HashGame.BindingModel;
using HashGame.Models;

namespace HashGame.Mapper
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            CreateMap<Game, CreateGameBindingModel>()
                .ForMember(g => g.FirstPlayer, opt => opt.MapFrom(source => source.PlayerMovements[0]))
                .ForMember(g => g.Id, opt => opt.MapFrom(source => source.Id));
        }        
    }
}
