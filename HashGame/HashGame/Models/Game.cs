﻿using System;
using System.Collections.Generic;

namespace HashGame.Models
{
    public class Game
    {
        public Guid Id { get; set; }
        public List<string> PlayerMovements { get; set; }
        public string[,] GameBoard { get; set; }

        public Game()
        {
            Id = Guid.NewGuid();
            PlayerMovements = new List<string>();
            GameBoard = new string[3, 3];
        }
    }
}
