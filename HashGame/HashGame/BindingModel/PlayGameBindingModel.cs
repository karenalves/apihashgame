﻿using HashGame.Models;
using System;

namespace HashGame.BindingModel
{
    public class PlayGameBindingModel
    {
        public Guid Id { get; set; }
        public string Player { get; set; }
        public Position Position { get; set; }

    }
}
