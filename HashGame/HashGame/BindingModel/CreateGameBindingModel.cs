﻿using System;

namespace HashGame.BindingModel
{
    public class CreateGameBindingModel
    {
        public Guid Id { get; set; }
        public string FirstPlayer { get; set; }
    }
}
